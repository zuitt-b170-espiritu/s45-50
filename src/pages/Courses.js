// Base imports
import React from 'react';
import CourseCard from '../components/CourseCard';


// Bootstrap dependencies
import Container from 'react-bootstrap/Container';


// Data Import
import courses from '../mock-data/courses';


export default function	Courses(){
	const CourseCards=courses.map((course)=>{
		return (
				<CourseCard course={course} />
			)
	})
	return (
		<Container fluid>
			{CourseCards}
		</Container>
	)
}