// dependencies
import React, {useState, useEffect} from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

export default function Course(props) {
    /*
        Props - short term for properties. similar to the arguments/parameters found inside the functions. a way for the parent component to receive information.

                thourgh the use of props, devs can use the same component and feed different information/data for rendering

    */
    let course = props.course;

    /*
        useState() a hook used in React to allow components to create manage its own data and is meant to be used internally. 
                            -accepts an argument that is meant to be the value of the first element in array

        in React.js, state values must not be changed directly. All changes to the state values must be through the setState() function 
                -setState() is the second element in the created array variable


        in the code below...
            the enrollees will start at zero. the result of the useState() is an array of data that is then destructured into count and setCount.

            setCount function is used to update the value of the count variable, depending on the times that the enroll function is triggered by the onClick command (button event)
    */

    const [ isDisabled, setIsDisabled ] = useState(0);
    const [ seats, setSeats ] = useState(30);

    useEffect(()=> {
        if (seats === 0) {
            setIsDisabled(true);
        }
    },[seats]);


    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>{course.price}</p>
                <h6>Seats</h6>
                <p>{seats} remaining</p>
                <Button variant="primary" onClick={() => setSeats(seats - 1)} disabled={isDisabled}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}

