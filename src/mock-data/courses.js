export default [
	{
		id:"wdc001",
		name:"PHP-LARAVEL",
		description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada nulla orci, et molestie neque scelerisque ac. Proin congue odio euismod, vehicula ex at, scelerisque purus.",
		price:45000,
		onOffer:true
	},
	{
		id:"wdc002",
		name:"Python-Django",
		description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada nulla orci, et molestie neque scelerisque ac. Proin congue odio euismod, vehicula ex at, scelerisque purus.",
		price:50000,
		onOffer:true
	},
	{
		id:"wdc003",
		name:"Java-Springboot",
		description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada nulla orci, et molestie neque scelerisque ac. Proin congue odio euismod, vehicula ex at, scelerisque purus.",
		price:55000,
		onOffer:true
	}

]